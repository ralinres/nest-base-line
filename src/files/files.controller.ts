import { Controller, Get, Post, Body, Patch, Param, Delete, UploadedFile, UseInterceptors, ParseFilePipe, MaxFileSizeValidator, FileTypeValidator } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { FilesService } from './files.service';

@Controller('files')
export class FilesController {
  constructor(private readonly filesService: FilesService) {}
    
   @Post('product')
   @UseInterceptors(FileInterceptor('file'))
   uploadProductFile( @UploadedFile(
    new ParseFilePipe({
      validators: [
        new MaxFileSizeValidator({ maxSize: 111000 }),
        new FileTypeValidator({ fileType: 'jpeg|png' }),
      ],
    })
   ) file: Express.Multer.File,){
    
     return file;

   }
}
