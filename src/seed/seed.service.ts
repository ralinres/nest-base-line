import { Injectable } from '@nestjs/common';
import { ProductsService } from '../products/products.service';
import { initialData } from './data/seed-data';

@Injectable()
export class SeedService {

  constructor(
    private readonly productService: ProductsService
  ){}

   async runSeed() {
    
    await this.insterNewProducts();
    return `seed executed`;
  }

  private async insterNewProducts(){
      
    await this.productService.deleteAllProducts();
    
    const seedProducts = initialData.products;

    const insterPromises = [];

    seedProducts.forEach( product => {
       insterPromises.push(this.productService.create(product));
    });

    const result = await Promise.all(insterPromises);
    return result;
  }

}
